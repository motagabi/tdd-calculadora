package com.br.tdd;

public class Calculadora {
    public static int soma(int primeiro , int segundo){
        int resultado = primeiro + segundo;
        return resultado;
    }

    public static int sub(int primeiro , int segundo){
        int resultado = primeiro - segundo;
        return resultado;
    }

    public static int mul(int primeiro , int segundo){
        int resultado = primeiro * segundo;
        return resultado;
    }

    public static int div(int primeiro , int segundo){
        int resultado = primeiro / segundo;
        return resultado;
    }
}
