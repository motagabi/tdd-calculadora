package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {
    @Test
    public void testarSomaDeDoisNumeros(){
        int resultado = Calculadora.soma(2 , 2);

        Assertions.assertEquals(4,resultado);
    }

    @Test
    public void testarSubDeDoisNumeros(){
        int resultado = Calculadora.sub(2,1);
        Assertions.assertEquals(1,resultado);
    }

    @Test
    public void testarMulDeDoisNumeros(){
        int resultado = Calculadora.mul(2,1);
        Assertions.assertEquals(2,resultado);
    }
    @Test
    public void testarDivDeDoisNumeros(){
        int resultado = Calculadora.div(4,2);
        Assertions.assertEquals(2,resultado);
    }
}
